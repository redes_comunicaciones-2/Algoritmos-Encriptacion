<?php
    class shannon_Fano {
        //Metodos y variables
        private $mensaje;
        private $subCaracteres,$frecuencias,$entropia,$letras,$tamaño,$mitades,$diferencias,$mitad,$probablidades,
                $codigosBin,$entropiaMsj,$bitsCodigo,$bitsMsj,$tablaFinal,$tablaHTML,$saltos,$bitsDatos,$comprimido,
                $ahorrado,$codificado,$bitsInicial,$mensajeCodificado,$tablaArbol,$arbol;
        private static $existe;
        private $n;
        //Constructor
        function __construct($m){
            $this->mensaje=$m;
        }
        //Metodo que inicializa el algoritmo Shanon-Fano
        public function iniciar(){
            //Creacion de la tabla
            $this->subCaracteres = array();
            $this->frecuencias = array();
            for($i=0;$i < strlen($this->mensaje);$i++){
                $c = $this->charAt($this->mensaje,$i);
                $this->frecuencia($c);
            }
            $this->tamaño = $this->tamaño();
            $this->crearMatriz($this->tamaño);
            $this->hallarFrecuencias();
            $this->ordenarPorFrecuencia();
            $this->llenarSimbolos_Frecuencias();
            //Para arbol y codigos binarios
            $this->mitad = $this->particionFrecuencias(0);
            $this->hallarCodigos($this->mitad);
            //$this->arbol = $this->crearApuntadores();
            //Completar tabla
            $this->calcularEntrp();
            $this->calcularEntropMsj();
            $this->bitsCodigo();
            $this->bitsMsj();
            $this->probAp();
            //Realizar tabla para mostrarla al usuario
            $this->hacerTablaHTML($this->tamaño);
            $this->bitsDatos = $this->calculoAhorro($this->mensaje);
            //Para realizar la codificacion del mensaje
            $this->mensajeCodificado = $this->codificacionMsj();
            //Determinacion de saltos '</br>' segun tamaño del mensaje
            $this->saltos = "</br>";
            $this->saltos .= $this->saltosBr(sizeOf($this->frecuencias));
        }

        //Funcion que devuleve el char de un string en x posicion
        private function charAt($mensaje,$i){
            return $mensaje{$i};
        }

        //Funcion que arma un array en funcion de las palabras no repetidas
        private function frecuencia($letra){
            shannon_Fano::$existe = false;
            for($i=0;$i<sizeof($this->subCaracteres);$i++){
                if($this->subCaracteres[$i] == $letra){
                    shannon_Fano::$existe = true;
                    break;
                }
            }
            if(!shannon_Fano::$existe){
                array_push($this->subCaracteres,$letra);
            }
        }

        //Funcion que retoma el tamaño del mensaje
        private function tamaño(){
            return sizeOf($this->subCaracteres);
        }

        //Funcion que determina el tamaño de la matriz letras
        private function crearMatriz($tamaño){
            for($i=0;$i<$tamaño;$i++){
                for($j=0;$j<6;$j++){
                    $this->letras[$i][$j] = null;
                }
            }
        }

        //Funcion que halla las frecuencias de cada letra
        private function hallarFrecuencias(){
            for($i=0;$i<sizeOf($this->subCaracteres);$i++){
                $cont = strLen($this->mensaje) - strLen(str_replace($this->subCaracteres[$i],"",$this->mensaje));
                $this->frecuencias[$i] = $cont;
            }
        }

        //Funcion que ordena segun el orden de frecuencia
        private function ordenarPorFrecuencia(){
            for($i=0;$i < sizeOf($this->subCaracteres)-1;$i++){
                for($j=0;$j < sizeOf($this->subCaracteres)-$i-1;$j++){
                    if($this->frecuencias[$j+1] < $this->frecuencias[$j]){
                        //Para la frecuencia
                        $aux = $this->frecuencias[$j+1];
                        $this->frecuencias[$j+1] = $this->frecuencias[$j];
                        $this->frecuencias[$j] = $aux;
                        //Para mantener el orden por simbolos
                        $tmp = $this->subCaracteres[$j+1];
                        $this->subCaracteres[$j+1] = $this->subCaracteres[$j];
                        $this->subCaracteres[$j] = $tmp;
                    }
                }
            }
            $this->frecuencias = array_reverse($this->frecuencias);
            $this->subCaracteres = array_reverse($this->subCaracteres);
        }

        //Funcion que llena los simbolos no repetidos del mensaje y frecuencias del mismo
        private function llenarSimbolos_Frecuencias(){
            for($i=0;$i<sizeOf($this->subCaracteres);$i++){
                $this->letras[$i][0] = $this->subCaracteres[$i];
                $this->letras[$i][1] = $this->frecuencias[$i];
            }
        }

        //Funcion que parte la lista de frecuencias para encontrar la minima diferencia entre las sumas superior
        //E inferior de la columna de frecuencias
        private function particionFrecuencias($v){
            echo "tamaño arreglo para particion de suma: ". $this->count($v);
            $mitad = round($this->count($v)/2);
            $middle = $mitad;
            $this->mitades = array();
            $this->diferencias = array();
            for($i=0;$i<$mitad;$i++){
                $this->mitades[$i] = $middle;
                $this->diferencias[$i] = abs($this->calcDiferencia($middle,$v));
                $middle--;
            }
            $middle = $mitad;
            for($i=$mitad;$i < sizeOf($this->frecuencias);$i++){
                $this->mitades[$i] = $middle;
                $this->diferencias[$i] = abs($this->calcDiferencia($middle,$v));
                $middle++;
            }
            $valorMin = min($this->diferencias);
            $mitad = $this->valorMitad($valorMin);
            return $mitad;
        }

        //Funcion contar
        private function count($v){
            $c=0;
            for($i=$v;$i<sizeOf($this->frecuencias);$i++){
                $c++;
            }
            return $c;
        }
        //Funcion que devuelve la diferencia entre las sumas, entre mas minima sea, mas optimo
        //Para hallar los codigos en binario de los simbolos
        private function calcDiferencia($mitad,$v){
            $sumSup = $this->sumSup($mitad);
            $sumInf = $this->sumInf($mitad,$v);
            return ($sumSup-$sumInf);
        }

        //Funcion que devuelve la suma inferior de la columna de frecuencias
        private function sumInf($mitad,$v){
            $sumInf=0;
            for($i=$v;$i < $mitad;$i++){
                $sumInf += $this->frecuencias[$i];
            }
            return $sumInf;
        }

        //Funcion que devulve la suma superior de la columna de frecuencias
        private function sumSup($mitad){
            $sumSup = 0;
            for($i=$mitad;$i < sizeOf($this->frecuencias);$i++){
                $sumSup += $this->frecuencias[$i];
            }
            return $sumSup;
        }

        //Funcion que returna la mitad del valor minimo
        private function valorMitad($v){
            for($i=0;$i<sizeOf($this->mitades);$i++){
                if($v == $this->diferencias[$i]){
                    return $this->mitades[$i];
                }
            }
        }

        //Funcion que halla los codigos en binario de la mitad inferior y superior
        //De la particion de la tabla realizada anteriormente
        private function hallarCodigos($mitad){
            $this->codigosBin = array();
            $bitsSig = $this->bitsNecesarios($mitad);
            $this->hallarBin($bitsSig,$mitad);
        }

        //Funcion que determina el minimo numero de bits para representar la primer mitad de la col frec.
        //Teniendo en cuenta los bits significativos del indice hasta la mitad calculada
        private function bitsNecesarios($mitad){
            return strlen(decbin("".$mitad));
        }

    //Funcion que llena los numeros binarios en funcion de la cantidad de bits significativos
    private function hallarBin($bitsSig,$mitad)
    {   
        //Para la mitad inferior
        for($i=0;$i<$bitsSig;$i++){
            $numeroBin = decbin($i);
            $ceros = "";
            for($j=0;$j<($bitsSig-strlen($numeroBin));$j++){
                $ceros .= "0";
            }
            $this->codigosBin[$i] = $ceros.$numeroBin;
        }
        //Para la mitad superior
        for($i=$mitad;$i<sizeOf($this->frecuencias);$i++){
            $unos = "";
        }
    }

        //Funcion que llena el arbol
        private function crearApuntadores(){
            $this->tablaArbol = array();
            $arrNum = array();
            $tamaño = strlen(max($this->codigosBin))+sizeOf($this->subCaracteres)+1;
            $cont = 1;
            for($i = 0;$i < sizeOf($this->subCaracteres)-2;$i++){
                $arrNum[$i] = $cont;
                $cont++;
            }

            $tamaño = sizeOf($this->subCaracteres) + sizeOf($arrNum);
            $c=0;
            for($i=0;$i<=$tamaño;$i++){
                if($i>=sizeOf($this->subCaracteres)){
                    if($i===$tamaño){
                        $this->tablaArbol[1][$i] = "raiz";
                    }else{
                        $this->tablaArbol[1][$i] = $arrNum[$c];
                        $c++;
                    }
                }else{
                    $this->tablaArbol[1][$i] = $this->subCaracteres[$i];
                }
                $this->tablaArbol[2][$i] = null;
                $this->tablaArbol[3][$i] = null;
                $this->tablaArbol[0][$i] = $i;
            }

            //Hacer recorridos en funcion de los numeros binarios
            $numeros = $this->hacerRecorridos();
            //Armar filas hijo izq(2) y der(3) de la tabla Arbol
            $this->armarHijos($numeros);
            $this->hacerApuntadores();
            return ($this->arbolHTML($tamaño));
        }

    //Funcion que estableces los recorridos para cada nodo por posicion del caracter o letra
    private function hacerRecorridos()
    {
        $contador=1;
        $string="";
        $numeros = array();
        for($i=0;$i<sizeOf($this->codigosBin);$i++){
            for($j=0;$j<strlen($this->codigosBin[$i]);$j++){
                //Para la primer posicion
                if($i===0){
                    $string = $this->subCaracteres[$i];
                }else{
                    if($i===sizeOf($this->codigosBin)){
                        $v = $numeros[$i-1];
                        $string = rtrim($v,$this->subCaracteres[$i]);
                    }else{
                        if($j===strlen($this->codigosBin[$i])-1){
                            $string .= $this->subCaracteres[$i];
                        }else{
                            $string .= $contador;
                            $contador++;
                        }
                    }
                }
            }
            $numeros[$i] = $string;
            $contador=1;
            $string = "";
        }
        return $numeros;
    }
    //Funcion que arma los padres e hijos izq /der del arbol
    private function armarHijos($numeros){
        if(sizeOf($this->subCaracteres)==1){
            $this->tablaArbol[2][1] = $this->subCaracteres[0];
        }else{
            $this->tablaArbol[2][sizeOf($this->tablaArbol[0])-1] = $numeros[0];
            $this->tablaArbol[3][sizeOf($this->tablaArbol[0])-1] = $numeros[1][0];
        }
        for($i=0;$i<sizeOf($numeros);$i++){
            for($j=0;$j<strlen($numeros[$i])-1;$j++){
                for($k=sizeOf($this->tablaArbol[0])-1;$k>=0;$k--){
                    if(strlen($numeros[$i])>1){
                        if($this->tablaArbol[1][$k] == $numeros[$i][$j]){
                                if($this->codigosBin[$i][$j+1]==="0"){
                                    $this->tablaArbol[2][$k] = $numeros[$i][$j+1];
                                    break;
                                }else{
                                    $this->tablaArbol[3][$k] = $numeros[$i][$j+1];
                                    break;
                                }
                        }
                    }
                }
            }
        }
    }

        //Funcion que reemplaza los valores por apuntadores
        private function hacerApuntadores(){
            //Para izq
            for($i=sizeOf($this->tablaArbol[2])-1;$i>=0;$i--){
                for($j=0;$j<sizeOf($this->tablaArbol[1]);$j++){
                    if($this->tablaArbol[1][$i] === $this->tablaArbol[2][$j]){
                        $this->tablaArbol[2][$j] = $this->tablaArbol[0][$i];
                        break;
                    }
                }
            }

            //Para der
            for($i=sizeOf($this->tablaArbol[3])-1;$i>=0;$i--){
                for($j=0;$j<sizeOf($this->tablaArbol[1]);$j++){
                    if($this->tablaArbol[1][$i] == $this->tablaArbol[3][$j]){
                        $this->tablaArbol[3][$j] = $this->tablaArbol[0][$i];
                        break;
                    }
                }
            }
        }

        //Funcion que hace el arbolHTML()
        private function arbolHTML($cabezaArbol){
            $arbol= '';
            if($cabezaArbol === null){
                return '<li><span>*</span></li>';
            }else{
                $izquierda = $this->arbolHTML($this->tablaArbol[2][$cabezaArbol]);
                $derecha = $this->arbolHTML($this->tablaArbol[3][$cabezaArbol]);
                $arbol = '<li>' .
                '<div ' . ($this->tablaArbol[1][$cabezaArbol] === null ? 'style="font-size: 0.9rem"' : '') . '><a><big><big><big><big><b>' .
                ($this->tablaArbol[1][$cabezaArbol] !== null ? ($this->tablaArbol[0][$cabezaArbol]) === 32 ? '&nbsp' : $this->tablaArbol[1][$cabezaArbol] : $this->tablaArbol[1][$cabezaArbol]) .
                '</b></big></big></big></big></a></div>';
                if($this->tablaArbol[2][$cabezaArbol] !== null ||$this->tablaArbol[3][$cabezaArbol] !== null ){
                    $arbol .= '<ul>'.$izquierda.$derecha.'</ul>';
                }
                $arbol .= '</li>';
            }
            return $arbol;
        }
        

        //Funcion que llena la columna de entropia de la tabla
        private function calcularEntrp(){
            $this->entropia = array();
            $x = 0;
            $largoSerie = $this->sumatoria($this->frecuencias);
            for($i=0;$i<sizeOf($this->frecuencias);$i++){
                $x = $largoSerie / $this->frecuencias[$i];
                $this->entropia[$i] = $this->redondear(log($x,2),3);
            }
        }

        //Funcion que llena la columna entropia del mensaje de la tabla
        private function calcularEntropMsj(){
            for($i=0;$i<sizeOf($this->frecuencias);$i++){
                $this->entropiaMsj[$i] = $this->redondear($this->entropia[$i]*$this->frecuencias[$i],3);
            }
        }

        //Funcion que redondea decimales
        private function redondear($numero,$decimales){
            $factor = pow(10,$decimales);
            return (round($numero*$factor)/$factor);
        }

        //Funcion para calcular sumatorias de algo
        private function sumatoria($arreglo){
            $suma=0;
            for($i=0;$i<sizeOf($arreglo);$i++){
                $suma += $arreglo[$i];
            }
            return $suma;
        }

        //Funcion que pone la columna bits codigo
        private function bitsCodigo(){
            $this->bitsCodigo = array();
            for($i=0;$i<sizeOf($this->codigosBin);$i++){
                $this->bitsCodigo[$i] = strlen($this->codigosBin[$i]);
            }
        }

        //Funcion que pone la columna bits mensaje
        private function bitsMsj(){
            $this->bitsMsj = array();
            for($i=0;$i<sizeOf($this->frecuencias);$i++){
                $this->bitsMsj[$i] = $this->bitsCodigo[$i]*$this->frecuencias[$i];
            }
        }

        //Funcion que calcula y pone en el arreglo la probabilidad de aparicion de un caracter
        private function probAp(){
            $this->probablidades = array();
            $total = $this->sumatoria($this->frecuencias);
            for($i=0;$i<sizeOf($this->frecuencias);$i++){
                $this->probabilidades[$i] = $this->redondear($this->frecuencias[$i]/$total,3);
            }
        }


        //Funcion para finalizar tabla a mostrar al usuario
        private function hacerTablaHTML($tamaño){
            $this->tablaFinal = array();
            for($i=0;$i<$tamaño+2;$i++){
                for($j=0;$j<7;$j++){
                    if($i==0){
                        if($j==0){
                            $this->tablaFinal[$i][$j] = "S&iacute;mbolo";
                        }else if($j==1){
                            $this->tablaFinal[$i][$j] = "Frecuenc&iacute;a";
                        }else if($j==2){
                            $this->tablaFinal[$i][$j] = "Probabilidad";
                        }else if($j==3){
                            $this->tablaFinal[$i][$j] = "Entrop&iacute;a";
                        }else if($j==4){
                            $this->tablaFinal[$i][$j] = "Entrop&iacute;a msj";
                        }else if($j==5){
                            $this->tablaFinal[$i][$j] = "Bits c&oacute;digo";
                        }else if($j==6){
                            $this->tablaFinal[$i][$j] = "Bits mensaje";
                        }
                    } else if($i!= 0 && $i < $tamaño+1){
                        if($j==0){
                            $this->tablaFinal[$i][$j] = $this->subCaracteres[$i-1];
                        }else if($j==1){
                            $this->tablaFinal[$i][$j] = $this->frecuencias[$i-1];
                        }else if($j==2){
                            $this->tablaFinal[$i][$j] = $this->probabilidades[$i-1];
                        }else if($j==3){
                            $this->tablaFinal[$i][$j] = $this->entropia[$i-1];
                        }else if($j==4){
                            $this->tablaFinal[$i][$j] = $this->entropiaMsj[$i-1];
                        }else if($j==5){
                            $this->tablaFinal[$i][$j] = $this->bitsCodigo[$i-1];
                        }else if($j==6){
                            $this->tablaFinal[$i][$j] = $this->bitsMsj[$i-1];
                        }
                    }else{
                        if($j==0){
                            $this->tablaFinal[$i][$j] = "TOTAL";
                        }else if($j==1){
                            $this->tablaFinal[$i][$j] = $this->sumatoria($this->frecuencias);
                        }else if($j==2){
                            $this->tablaFinal[$i][$j] = $this->redondear($this->sumatoria($this->probabilidades),2);
                        }else if($j==3){
                            $this->tablaFinal[$i][$j] = $this->sumatoria($this->entropia);
                        }else if($j==4){
                            $this->tablaFinal[$i][$j] = $this->sumatoria($this->entropiaMsj);
                        }else if($j==5){
                            $this->tablaFinal[$i][$j] = $this->sumatoria($this->bitsCodigo);
                        }else if($j==6){
                            $this->tablaFinal[$i][$j] = $this->sumatoria($this->bitsMsj);
                        }
                    }
                }
            }
            //para impresion HTML
            $this->tablaHTML = "";
            for ($i = 0; $i < $tamaño+2; $i++) {
                $this->tablaHTML .= "<tr>";
                    for ($j = 0; $j < 7; $j++) {
                        if ($i == 0) {
                            $this->tablaHTML .= "<th><h3><big>".$this->tablaFinal[$i][$j]."</big></h3></th>";
                        } else if ($i == $tamaño+1) {
                            $this->tablaHTML .= "<th><h3><big>".$this->tablaFinal[$i][$j]."</big></h3></th>";
                        } else {
                            $this->tablaHTML .= "<td><big>".$this->tablaFinal[$i][$j]."</big></td>";
                        }
                    }
                    $this->tablaHTML .= "</tr>";
            }
        }

        //Funcion que calcula lo ahorrado
        //Funcion que calcula el ahorro y muestra informacion de la codificacion del mensaje
        private function calculoAhorro($mensaje){
            $this->bitsInicial = strlen($mensaje)*8;
            $bitsFinal = $this->tablaFinal[$this->tamaño+1][5];
            $append = "<i>Comprimido</i>: ".$bitsFinal." bits.</br>";
            $append .= '<p style="color:#007bff;"><i>Codificado</i>: '.$this->bitsCodificados($this->bitsAhorrados($this->bitsComprimido($this->bitsInicial,$bitsFinal),$this->bitsInicial))."%.</p>";
            $append .= '<p style="color:#04da36;"><i>Ahorrado</i>: '.$this->bitsAhorrados($this->bitsComprimido($this->bitsInicial,$bitsFinal),$this->bitsInicial)."%.</p>";
            $append2 = "<i>Bits iniciales</i>: ".strlen($this->mensaje). " (caracteres del mensaje) * 8 (ASCII) = ".intval($this->bitsInicial)." bits.</br>";
            return $append2.$append;
        }

        //funcion que calcula los bits comprimidos
        private function bitsComprimido($a,$b){
            $this->comprimido = intval($a-$b);
            return $this->comprimido;
        }

        //Funcion que calcula los bits ahorrados
        private function bitsAhorrados($a,$b){
            $this->ahorrado = $this->redondear((($a*100)/$b),3);
            return $this->ahorrado;
        }

        //Funcion que calcula los bits codificados
        private function bitsCodificados($a){
            $this->codificado = $this->redondear(100 - $a,3);
            return $this->codificado;
        }

        //Funcion que codifica el mensaje
        private function codificacionMsj(){
            $variable="";
            //Para agregar el mensaje escrito
            for($i=0;$i<sizeOf($this->subCaracteres);$i++){
                $variable .= $this->subCaracteres[$i] . " ";
            }
            $variable.="</br>";
            //Para agregar los numeros bin
            for($i=0;$i<sizeOf($this->subCaracteres);$i++){
                $variable .= $this->codigosBin[$i];
            }
            $variable.="</br>";
            //Para compara valores
            for($i=0;$i<sizeOf($this->subCaracteres);$i++){
                if($i == sizeOf($this->subCaracteres)-1){
                    $variable .= $this->subCaracteres[$i] . " = " . $this->codigosBin[$i];
                }else{
                    $variable .= $this->subCaracteres[$i] . " = " . $this->codigosBin[$i] .", ";
                }
            }
            return $variable;
        }

        //Funcion que determina los saltos '</br> en funcion del tamaño del mensaje'
        //Con el objetivo de no superponer elementos HTML
        private function saltosBr($tamaño){
            // tamaño con 7 se superpone, y apartir de este numero, cada 1 en tamaño un br
            if($tamaño > 7){
                for ($i=7;$i<$tamaño;$i++){
                    if($i%3==0){
                        $this->saltos .= "</br>";
                    }
                }
            }
            return $this->saltos;
        }

        //Funcion para imprimir arreglo
        private function imprimirArreglo($tabla){
            for($x=0;$x<sizeOf($tabla);$x++){
                echo $tabla[$x];
                echo "</br>";
            }
        }

        //Funcion para imprimir matriz
        private function imprimirMatriz($tabla){
            for($x=0;$x<sizeOf($tabla);$x++){
                echo "</br>";
                for($y=0;$y<sizeOf($tabla[$x]);$y++){
                    echo $tabla[$x][$y] ."&nbsp;";
                }
            }
        }

        //Getters

        public function getTablaHTML(){
            return $this->tablaHTML;
        }

        public function getSaltos(){
            return $this->saltos;
        }

        public function getBitsDatos(){
            return $this->bitsDatos;
        }

        public function getGraficoBarra(){
            $html = '<ul class="chart1"><li>
                        <span style="height:'.$this->codificado.'%" title="Codificado">'.$this->codificado.'%</span>
                        </li><ul class="chart2"><li>
                        <span style="height:'.$this->ahorrado.'%" title="Ahorrado">'.$this->ahorrado.'%</span>
                        </li></ul></ul>';
            return $html;
        }

        public function getAhorrado(){
            return $this->ahorrado;
        }

        public function getCodificado(){
            return $this->codificado;
        }

        public function getArbol(){
            return $this->arbol;
        }

        public function getMesjCod(){
            return $this->mensajeCodificado;
        }
        
    }

$tabla;
$saltos;
$bitsDatos;
$graficoBarra;
$codificado;
$ahorrado;
$arbol;
$saltosA;
$mensajeCodificado;
//Declaracion variables POST y validacion
if(!empty($_POST['mensaje'])){
    $mensaje = $_POST['mensaje'];
    $p = new shannon_Fano($mensaje);
    $p->iniciar();
    $tabla = $p->getTablaHTML();
    $saltos = $p->getSaltos();
    $bitsDatos = $p->getBitsDatos();
    $graficoBarra = $p->getGraficoBarra();
    $codificado = $p->getCodificado();
    $ahorrado = $p->getAhorrado();
    $arbol = $p->getArbol();
    $saltosA = $saltos.$saltos.$saltos;
    $mensajeCodificado = $p->getMesjCod();
}else{
    $mensaje = "Porvafor escriba un mensaje para codificar";
}
    
require'../views/shannonFano.view.php'
?>